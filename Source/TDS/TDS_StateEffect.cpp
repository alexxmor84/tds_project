// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "TDS/TDS_HealthComponent.h"
#include "TDS/TDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogStateEffect, All, All);

bool UTDS_StateEffect::InitObject(AActor* Actor, FName HitBoneName, FVector PointHit)
{
	myActor = Actor;
	BoneName = HitBoneName;
	ImpactPoint = PointHit;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if(myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if(myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName HitBoneName, FVector PointHit)
{
	Super::InitObject(Actor, HitBoneName, PointHit);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{

	if (myActor)
	{
		UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));

		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName HitBoneName, FVector PointHit)
{
	Super::InitObject(Actor, HitBoneName, PointHit);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		
		FName NameBoneToAttached = HitBoneName;
		FVector Location = PointHit;

		USceneComponent* StaticMesh = Cast<USceneComponent>(myActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));

		if (StaticMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, StaticMesh, NameBoneToAttached, Location, FRotator::ZeroRotator, EAttachLocation::KeepWorldPosition, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, FVector(0), FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}

		UE_LOG(LogStateEffect, Warning, TEXT("Bone: %s"), *NameBoneToAttached.ToString());
	}

	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;

	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::AppleDamage(myActor, Power, nullptr, nullptr);
		UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));

		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentValue(Power);
		}
	}
}
