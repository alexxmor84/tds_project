// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSGameMode.h"
#include "TDSPlayerController.h"
#include "TDS/Character/TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSGameMode::ATDSGameMode()
{
	 /*use our custom PlayerController class*/
	PlayerControllerClass = ATDSPlayerController::StaticClass();

	/*set default pawn class to our Blueprinted character*/
	FString ClassPath = "/Game/Blueprint/Character/BP_Character";
	const TCHAR* Class = *ClassPath;
	UClass* ActorClass = LoadObject<UClass>(nullptr, Class);

}

//void ATDSGameMode::PlayerCharacterDead()
//{}